﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TileGenerator : MonoBehaviour
{
    public GameObject tilePrefab;

    public List<char> letterPool;
    public Dictionary<char, int> letterFrequencyMap;

    Action<List<TileInstance>> onTilesDrawn;

    private void Awake()
    {
        letterFrequencyMap = new Dictionary<char, int>();
    }

    public void RegisterTileDrawObserver(Action<List<TileInstance>> observer) {
        onTilesDrawn += observer;
    }

    public void UnregisterTileDrawObserver(Action<List<TileInstance>> observer) {
        onTilesDrawn -= observer;
    }

    public void CreateLetterFrequencyList(string rawText)
    { 
        string[] letterWithFrequency = rawText.Trim().Split('\n');
        
        foreach (string line in letterWithFrequency)
        {
            string[] data = line.Split(' ');
            char letter = char.Parse(data[0]);
            int amount = int.Parse(data[1]);
            letterFrequencyMap.Add(letter, amount);
        }
    }

    public void ResetLetterPool() {
        letterPool.Clear();
        foreach (char letter in letterFrequencyMap.Keys) {
            AddLetterToPool(letter, letterFrequencyMap[letter]);
        }
    }

    public void AddLetterToPool(char letter, int quantity)
    {
        for (int i = 0; i < quantity; i++)
        {
            letterPool.Add(letter);
        }
    }

    public TileInstance GenerateTile()
    {
        int index = UnityEngine.Random.Range(0, letterPool.Count);

        char letter = letterPool[index];
        letterPool.RemoveAt(index);

        return GenerateSpecificTile(letter);
    }

    public TileInstance GenerateSpecificTile(char letter)
    {
        GameObject tileObj = Instantiate(tilePrefab);

        TileInstance tile = tileObj.GetComponent<TileInstance>();

        tile.letter = letter;
        tile.layerType = LayerType.Tile;
        tileObj.name = letter.ToString();

        return tile;
    }

    
    List<TileInstance> DrawTiles(string given)
    {
        List<TileInstance> tiles = new List<TileInstance>();

        for (int i = 0; i < given.Length; i++)
        {
            TileInstance t = GenerateSpecificTile(given[i]);
            tiles.Add(t);
        }

        onTilesDrawn?.Invoke(tiles);

        return tiles;
    }

    public List<TileInstance> DrawTiles(int num)
    {
        List<TileInstance> tiles = new List<TileInstance>();

        for (int i = 0; i < num; i++)
        {
            TileInstance t = GenerateTile();
            tiles.Add(t);
        }

        onTilesDrawn?.Invoke(tiles);

        return tiles;
    }

}
