﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionLogView : MonoBehaviour
{
    public GameObject logMessageInstancePrefab;
    public GameObject messageContainer;
    public ScrollRect scrollRect;
    public List<GameObject> messageList;

    private void Start()
    {
        messageList = new List<GameObject>();
    }

    public void AddEntry(string message, MessageType mType)
    {
        GameObject messageObj = Instantiate(logMessageInstancePrefab);
        LogMessageInstance logMessageInstance = messageObj.GetComponent<LogMessageInstance>();

        logMessageInstance.SetMessage(message, mType);

        messageObj.transform.SetParent(messageContainer.transform, false);
        messageList.Add(messageObj);

        // tell UI element that there is a new object to display in the ScrollRect
        // ignoring this step will cause the ScrollRect to miss the last message added
        LayoutRebuilder.ForceRebuildLayoutImmediate(messageContainer.GetComponent<RectTransform>());

        // set scrollbar and viewport to the bottom, the newest message
        scrollRect.verticalNormalizedPosition = 0.0f;
        scrollRect.verticalScrollbar.value = 0f;
    
    }

    public void Clear() {
        foreach (GameObject obj in messageList) {
            Destroy(obj);
        }
        messageList.Clear();
        
    }
}
