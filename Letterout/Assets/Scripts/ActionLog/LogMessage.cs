﻿using UnityEngine;

public class LogMessage: ILogMessage
{

    static Color32 normalColor = new Color32(64, 64, 64, 255);
    static Color32 errorColor = new Color32(100, 0, 0, 255);
    static Color32 successColor = new Color32(0, 100, 0, 255);

    public string text { get; private set; }

    public Color32 color {get; private set;}

    private MessageType _messageType;
    public MessageType messageType {
        get {
            return _messageType; 
        } 
        private set {

            _messageType = value;

            switch (_messageType)
            {
                case MessageType.Error:
                    this.color = errorColor;
                    break;
                case MessageType.Success:
                    this.color = successColor;
                    break;
                case MessageType.Normal:
                default:
                    this.color = normalColor;
                    break;
            }
        }
    }

    public LogMessage() {
        this.messageType = MessageType.Normal;
    }

    public LogMessage(string text, MessageType mType) {
        SetMessage(text, mType);
    }

    public void SetMessage(string text, MessageType mType)
    {
        this.text = text;
        this.messageType = mType;
    }

}
