using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MessageType
{
    Normal,
    Error,
    Success
}

public interface ILogMessage 
{
    public string text { get; }

    public Color32 color {get;}

    public MessageType messageType {get;}

    public void SetMessage(string text, MessageType messageType);
}
