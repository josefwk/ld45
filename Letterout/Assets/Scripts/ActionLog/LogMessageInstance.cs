using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LogMessageInstance : MonoBehaviour, ILogMessage
{

    [SerializeField]
    TextMeshProUGUI textMesh;
    [SerializeField]
    Image backgroundImage;

    static Color32 normalColor = new Color32(64, 64, 64, 255);
    static Color32 errorColor = new Color32(100, 0, 0, 255);
    static Color32 successColor = new Color32(0, 100, 0, 255);

    public string text { 
        get {
            return textMesh.text;
        }
    }

    public Color32 color { 
        get {
            return backgroundImage.color;
        }
        set {
            backgroundImage.color = value;
        }
    }

    private MessageType _messageType;
    public MessageType messageType {
        get {
            return _messageType; 
        } 
        private set {

            _messageType = value;

            switch (_messageType)
            {
                case MessageType.Error:
                    this.color = errorColor;
                    break;
                case MessageType.Success:
                    this.color = successColor;
                    break;
                case MessageType.Normal:
                default:
                    this.color = normalColor;
                    break;
            }
        }
    }


    public void SetMessage(string text, MessageType messageType)
    {
        this.textMesh.text = text;
        this.messageType = messageType;
    }

}
