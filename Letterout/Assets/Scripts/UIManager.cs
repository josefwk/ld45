using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class UIManager : MonoBehaviour
{
    public Button submitButton;
    public Button helpButton;
    public GameObject helpPanel;
    public GameObject gameOverPanel;
    public LegendPanel legendPanel;
    public TextMeshProUGUI gameOverText;
    public TextMeshProUGUI mapNameLabel;

    public ScoreView scoreView;
    public ScoreView highScoreView;
    public ActionLogView actionLogView;
    public TileCounterView tileCounterView;
    public DiscardMeterView healthMeter;

    private string submitButtonText = "Submit Word";


    void Start() {

    }

    public void Init() {
        submitButton.onClick.RemoveAllListeners();
        helpButton.onClick.AddListener(() => helpPanel.SetActive(!helpPanel.activeSelf));
        SetScore(0);
    }

    public void RegisterSubmitButtonObserver(UnityAction onButtonClicked) {
        submitButton.onClick.AddListener(onButtonClicked);
    }

    public void ShowGameOverScreen(string message) {
        gameOverText.text = message;
        gameOverPanel.SetActive(true);

        submitButton.GetComponentInChildren<TextMeshProUGUI>().text = "Restart Game";
        submitButton.onClick.RemoveAllListeners();
        submitButton.onClick.AddListener(() => RestartGame());
    }

    public void SetLevelHeading(string message) {
        mapNameLabel.text = message;
    }

    public void SetScore(int value) {
        scoreView.SetScore(value);
    }

    public void SetHighScore(int value) {
        highScoreView.SetScore(value);
    }

    public void DisplayLogMessage(string text, MessageType messageType) {
        actionLogView.AddEntry(text, messageType);
    }

    public void UpdateTileCounter(int value) {
        tileCounterView.SetDisplay(value);
    }

    public void UpdateLegendPanel() {
        if (legendPanel.isActiveAndEnabled)
            legendPanel.SetLegendPanel();
    }

    public void RestartGame() {
        actionLogView.Clear();
        submitButton.GetComponentInChildren<TextMeshProUGUI>().text = submitButtonText;
        GameManager.instance.StartGame();
        gameOverPanel.SetActive(false);
    }
}
