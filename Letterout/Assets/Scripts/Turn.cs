﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turn
{
    public List<TileData> placedTileData { get; private set; }

    public Turn()
    {
        placedTileData = new List<TileData>();
    }

    public void AddTilePlacement(TileData tileData)
    {
        placedTileData.Add(tileData);
    }

    public void RemoveTilePlacement(TileData tileData)
    {
        if (placedTileData.Contains(tileData))
        {
            placedTileData.Remove(tileData);
        }
    }
}
