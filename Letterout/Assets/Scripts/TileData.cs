using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileData
{
    // position data and letter data are requested by other classes, but no position data stored here
    // those classes do tile.cell.x, tile.cell.y
    private char _letter;
    public char letter {
        get {
            return _letter;
        } 
        set {
            _letter = value;
            this.pointValue = GetValueOfLetter(letter);
        }
    }
    public int x, y;
    public int pointValue { get; set; }
    public CellType placedOnCellType {get; set;}
    public bool isCommittedToBoard;

    public TileData() {

    }

    public TileData(int x, int y, char letter) {
        this.x = x;
        this.y = y;
        this.letter = letter;
    }

    int GetValueOfLetter(char letter)
    {
        switch (letter)
        {
            case 'A':
            case 'E':
            case 'I':
            case 'L':
            case 'N':
            case 'O':
            case 'R':
            case 'S':
            case 'T':
            case 'U':
                return 1;

            case 'D':
            case 'G':
            case 'M':
                return 2;

            case 'B':
            case 'C':
            case 'P':
                return 3;

            case 'F':
            case 'H':
            case 'V':
            case 'W':
            case 'Y':
                return 4;

            case 'K':
                return 5;

            case 'J':
            case 'X':
                return 8;

            case 'Q':
            case 'Z':
                return 10;

            default:
                return 0;
        }
    }
}
