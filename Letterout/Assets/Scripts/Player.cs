﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : IDamageable
{
   
    private int _maxHealth;
    public int maxHealth { 
        get {
            return _maxHealth; 
        } 
        private set {
            _maxHealth = value;
        } 
    }
    private int _currentHealth;
    public int currentHealth { 
        get {
            return _currentHealth; 
        } 
        private set {
            _currentHealth = value;
        } 
    }

    public int score { get; private set; }
    public int highScore { get; private set; }
    
    Action<int,int> onHealthChanged;
    Action<int> onScoreChanged;
    Action<int> onHighScoreChanged;
    Action onHealthZeroed;
    Action<string, MessageType> loggableEventHandler;


    public Player(int startingHealth, Action<string, MessageType> logHandler) {
        this.score = 0;
        this.maxHealth = startingHealth;
        this.currentHealth = startingHealth;
        this.loggableEventHandler += logHandler;
        SetHighScore(APIGateway.profile.highScore);
    }

    public void ModifyScore(int delta) {
        this.score += delta;
        AchievementObserver.ObserveScoreEvent(this.score);
        if (APIGateway.profile.highScore < this.score) {
            SetHighScore(this.score);
        }
        onScoreChanged?.Invoke(this.score);
    }

    public void SetHighScore(int value) {
        APIGateway.profile.highScore = value;
        onHighScoreChanged?.Invoke(value);
    }

    public void ModifyHealth(int delta) {
        if (delta < 0) {
            loggableEventHandler?.Invoke($"Took {Mathf.Abs(delta)} damage.", MessageType.Error);
            currentHealth = Mathf.Max(0, _currentHealth + delta);
        }
        else if (delta > 0) {
            loggableEventHandler?.Invoke($"Received {Mathf.Abs(delta)} health.", MessageType.Success);
            _currentHealth = Mathf.Min(maxHealth, _currentHealth + delta);
            APIGateway.profile.healthGained += delta;
            AchievementObserver.ObserveHealthGainEvent(APIGateway.profile.healthGained);
        }
        onHealthChanged?.Invoke(this.currentHealth, this.maxHealth);
        if (currentHealth <= 0) {
            onHealthZeroed?.Invoke();
        }
    }

    public void RegisterHealthChangedObserver(Action<int,int> callback)
    {
        onHealthChanged += callback;
    }
    public void UnregisterHealthChangedObserver(Action<int,int> callback)
    {
        onHealthChanged -= callback;
    }
    public void RegisterHealthZeroedObserver(Action callback)
    {
        onHealthZeroed += callback;
    }
     public void UnregisterHealthZeroedObserver(Action callback)
    {
        onHealthZeroed -= callback;
    }

    public void RegisterScoreChangedObserver(Action<int> callback)
    {
        onScoreChanged += callback;
    }
    public void UnregisterScoreChangedObserver(Action<int> callback)
    {
        onScoreChanged -= callback;
    }

    public void RegisterHighScoreChangedObserver(Action<int> callback)
    {
        onHighScoreChanged += callback;
    }
    public void UnregisterHighScoreChangedObserver(Action<int> callback)
    {
        onHighScoreChanged -= callback;
    }

    public void UnregisterAllObservers() {
        onHealthChanged = null;
        onScoreChanged = null;
        onHighScoreChanged = null;
        onHealthZeroed = null;
        loggableEventHandler = null;
    }

}
