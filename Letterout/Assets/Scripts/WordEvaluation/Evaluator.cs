﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Evaluator
{

    public Evaluator() {
        
    }

    public Evaluator(Action<string, MessageType> loggableEventHandler) {
        this.loggableEventHandler = loggableEventHandler;
    }

    Action<string, MessageType> loggableEventHandler;

    /* Given an initial tile, get all neighboring tiles in a given direction, ignoring order */
    private HashSet<TileData> GetNeighbors(Board board, TileData tile, Direction direction, ref HashSet<TileData> tiles)
    {
        TileInstance neighbor = board.GetNeighboringTile(tile.x, tile.y, direction);

        if (neighbor == null) return tiles;

        tiles.Add(neighbor.data);

        return GetNeighbors(board, neighbor.data, direction, ref tiles);
    }

    private HashSet<TileData> GetVerticalWordTiles(Board board, TileData tile)
    {
        HashSet<TileData> tiles = new HashSet<TileData>();

        GetNeighbors(board, tile, Direction.North, ref tiles);
        GetNeighbors(board, tile, Direction.South, ref tiles);

        tiles.Add(tile);

        return tiles;
    }

    private HashSet<TileData> GetHorizontalWordTiles(Board board, TileData tile)
    {
        HashSet<TileData> tiles = new HashSet<TileData>();

        GetNeighbors(board, tile, Direction.East, ref tiles);
        GetNeighbors(board, tile, Direction.West, ref tiles);

        tiles.Add(tile);

        return tiles;
    }

    private List<TileData> SortTilesIntoWord(HashSet<TileData> tiles)
    {
        List<TileData> sortedTiles = tiles.OrderBy(
                            (tile) => tile.x)
                            .ThenBy((tile) => -tile.y)
                            .ToList();

        return sortedTiles;
    }

    private int GetWordScore(List<TileData> t)
    {
        int score = 0;
        int multiplier = 1;

        foreach (TileData tile in t)
        {
            // if tile was previously committed, only use tile's score value
            if (tile.isCommittedToBoard) {
                score += tile.pointValue;
                continue;
            };

            int value = tile.pointValue;

            switch (tile.placedOnCellType)
            {
                case CellType.DoubleLetter:
                    value *= 2;
                    break;
                case CellType.DoubleScore:
                    multiplier *= 2;
                    break;
                case CellType.TripleLetter:
                    value *= 3;
                    break;
                case CellType.TripleScore:
                    multiplier *= 3;
                    break;
                case CellType.QuadLetter:
                    value *= 4;
                    break;
                case CellType.QuadScore:
                    multiplier *= 4;
                    break;
                default:
                case CellType.Basic:
                    break;
            }

            score += value;
        }

        score *= multiplier;

        return score;
    }

    private int GetHealthDelta(List<TileData> t)
    {
        int healthDelta = 0;
        int multiplier = 1;

        foreach (TileData tile in t)
        {
            // ignore all health bonuses if the tile was already scored in previous round
            if (tile.isCommittedToBoard) continue;

            switch (tile.placedOnCellType)
            {
                case CellType.HealthRegen:
                    healthDelta += tile.pointValue;
                    break;
                case CellType.DoubleScore:
                    multiplier *= 2;
                    break;
                case CellType.TripleScore:
                    multiplier *= 3;
                    break;
                case CellType.QuadScore:
                    multiplier *= 4;
                    break;
                default:
                case CellType.Basic:
                    break;
            }
        }

        healthDelta *= multiplier;

        return healthDelta;
    }

    bool IsTilePlacementValid(Board board, List<TileData> tiles)
    {
        int numTiles = tiles.Count;
        bool isConnectedToExistingTiles = board.HasConnectionToAnExistingTile(tiles);
        bool isUsingStartTile = board.HasPlacementOnStartTile(tiles);

        if (numTiles == 0)
        {
            return false;
        }

        if (numTiles == 1 && isUsingStartTile == true)
        {
            loggableEventHandler?.Invoke("Words on start location must be at least two letters long!", MessageType.Error);
            return false;
        }

        if (isConnectedToExistingTiles == false && isUsingStartTile == false)
        {
            loggableEventHandler?.Invoke("Play on start location or next to existing letter tile!", MessageType.Error);
            return false;
        }

        if (board.IsTileSetInLine(tiles) == false) {
            loggableEventHandler?.Invoke("Place tiles in a vertical or horizontal line!", MessageType.Error);
            return false;
        }
    
        if (board.IsTileSetWithoutGaps(tiles) == false) {
            loggableEventHandler?.Invoke("All letters must be connected!", MessageType.Error);
            return false;
        }

        return true;       
    }

    public Tuple<int,int> EvaluatePlacedTiles(Board board, List<TileData> placedTiles)
    {
        int totalScore = 0;
        int healthDelta = 0;

        Tuple<int, int> result = new Tuple<int, int>(totalScore, healthDelta);

        HashSet<CompleteWord> completeWords = GetCompleteWordsFromTiles(board, placedTiles);

        if (completeWords.Count == 0) return result;

        foreach (CompleteWord word in completeWords)
        {
           if (Wordlist.ContainsWord(word.wordString) == false)
            {
                loggableEventHandler?.Invoke($"The word {word.wordString} is not valid.", MessageType.Error);
                return result;
            }
        }

        foreach (CompleteWord word in completeWords)
        {
            int wordScore = GetWordScore(word.orderedTiles);
            totalScore += wordScore;

            int healthChange = GetHealthDelta(word.orderedTiles); // currently unused
            healthDelta += healthChange; // currently unused

            AchievementObserver.ObserveWordSubmittedEvent(word.orderedTiles.Count);

            loggableEventHandler?.Invoke($"{word.wordString} scored {wordScore} points!", MessageType.Success);

        }
        
        result = new Tuple<int, int>(totalScore, healthDelta);;

        return result;
    }

    private HashSet<CompleteWord> GetCompleteWordsFromTiles(Board board, List<TileData> placedTiles) {

        HashSet<CompleteWord> completeWords = new HashSet<CompleteWord>();

        if (IsTilePlacementValid(board, placedTiles) == false) return completeWords;

        foreach (TileData tile in placedTiles)
        {
            HashSet<TileData> wordTiles = GetHorizontalWordTiles(board, tile);
            List<TileData> word = SortTilesIntoWord(wordTiles);

            if (word.Count < 2) continue;

            CompleteWord w = new CompleteWord(word);

            if (completeWords.Contains(w, new CompleteWordComparer()) == false)
            {
                completeWords.Add(w);
            }
        }

        foreach (TileData tile in placedTiles)
        {
            HashSet<TileData> wordTiles = GetVerticalWordTiles(board, tile);
            List<TileData> word = SortTilesIntoWord(wordTiles);

            if (word.Count < 2) continue;

            CompleteWord w = new CompleteWord(word);

            if (completeWords.Contains(w, new CompleteWordComparer()) == false)
            {
                completeWords.Add(w);
            }
        }

        return completeWords;
    }
}
