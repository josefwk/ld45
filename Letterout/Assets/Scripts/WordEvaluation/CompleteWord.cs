﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


struct CompleteWord
{
    public int xStart;
    public int xEnd;
    public int yStart;
    public int yEnd;
    public List<TileData> orderedTiles;
    public string wordString;

    public CompleteWord(List<TileData> orderedTiles)
    {
        this.orderedTiles = orderedTiles;
        xStart = orderedTiles[0].x;
        yStart = orderedTiles[0].y;
        xEnd = orderedTiles[orderedTiles.Count - 1].x;
        yEnd = orderedTiles[orderedTiles.Count - 1].y;
        wordString = "";

        for (int i = 0; i < orderedTiles.Count; i++)
        {
            wordString += orderedTiles[i].letter;
        }
    }

}

class CompleteWordComparer : IEqualityComparer<CompleteWord>
{
    public bool Equals(CompleteWord c1, CompleteWord c2)
    {
        int strCompare = string.Compare(c1.wordString, c2.wordString);
        if (strCompare != 0)
        {
            return false;
        }

        if (c1.xStart == c2.xStart && c1.yStart == c2.yStart)
            return true;
        else return false;
    }

    public int GetHashCode(CompleteWord obj)
    {
        throw new System.NotImplementedException();
    }
}