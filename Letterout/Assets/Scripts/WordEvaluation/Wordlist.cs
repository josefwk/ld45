﻿using System.Collections;
using System.Collections.Generic;

public class Wordlist
{
    public static HashSet<string> words = new HashSet<string>();

    public static bool isLoadingList = false;

    public static void AddWordToList(string word) {
        string transformedWord = TransformInputString(word);
        words.Add(transformedWord);
    }

    public static bool ContainsWord(string word) {
        string transformedWord = TransformInputString(word);
        return words.Contains(transformedWord);
    }

    public static void RemoveWordFromList(string word) {
        words.Remove(word);
    }

    public static void ClearList() {
        words.Clear();
    }

    private static string TransformInputString(string input) {
        string transformed = input.ToUpper().Trim();
        return transformed;
    }

    
    public static void CreateWordList(string rawText)
    {
        isLoadingList = true;

        string[] splitWords = rawText.Split('\n');
        
        foreach (string s in splitWords)
        {
            AddWordToList(s);
        }

        isLoadingList = false;
    }

}
