﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TileCounterView : MonoBehaviour
{
    public TextMeshPro counterDisplay;

    public void SetDisplay(int value)
    {
        counterDisplay.text = value.ToString();
    }
}
