﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SoundManager : MonoBehaviour
{

    public AudioClip putTile;
    public AudioClip getTile;
    public AudioClip error;
    public AudioClip destroyTile;
    public AudioClip exitLevel;
    public AudioClip pointGain;

    public AudioSource speakerSFX;

    public void PlaySFX(SFX sfx)
    {
        AudioClip clipToPlay = null;

        switch (sfx)
        {
            case SFX.PutTile:
                clipToPlay = putTile;
                break;
            case SFX.GetTile:
                clipToPlay = getTile;
                break;
            case SFX.DestroyTile:
                clipToPlay = destroyTile;
                break;
            case SFX.ExitLevel:
                clipToPlay = exitLevel;
                break;
            case SFX.PointGain:
                clipToPlay = pointGain;
                break;
            case SFX.Error:
                clipToPlay = error;
                break;
            default:
                break;
        }

        speakerSFX.clip = clipToPlay;
        speakerSFX.Play();
    }

    public IEnumerator PlaySFX(SFX sfx, int repeat)
    {
        int num = 0;
        while (num < repeat)
        {
            PlaySFX(sfx);

            yield return new WaitUntil(() => speakerSFX.isPlaying == false);
            num++;
        }

        yield return null;
    }
}
