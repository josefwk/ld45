﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;
using UnityEngine.UI;
using System;
using System.IO;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject boardContainer;
    public GameObject placedTileContainer;
    public GameObject rackContainer;

    // Top level control modules
    [SerializeField]
    private FileIO fileHandler;
    [SerializeField]
    private TileGenerator tileGenerator;
    [SerializeField]
    private SoundManager soundManager;
    [SerializeField]
    private UIManager uiManager;

    public MapGenerator mapGenerator;
    public DiscardHandler discardHandler;
        
    Evaluator evaluator;
    Player player;

    Turn currentTurn;
    List<Turn> turns;
    public List<TileInstance> lettersOnRack;

    private int randomSeed;
    
    private void HandleLoggableEvent(string text, MessageType messageType) {
        
        uiManager.DisplayLogMessage(text, messageType);
        switch (messageType) {
            case MessageType.Error:
                soundManager.PlaySFX(SFX.Error);
                break;
            case MessageType.Normal:
            case MessageType.Success:
            default:
                break;
        }
    }

    private void HandleLastMapCompletedEvent() {
        string message = "Congratulations! You made it past the last level. " +
                         "Thanks for playing!";
        uiManager.ShowGameOverScreen(message);

        APIGateway.profile.wins++;
        AchievementObserver.ObserveGameWinEvent(APIGateway.profile.wins);
        APIGateway.PostData(APIGateway.profile);
    }

   
    private void Start() {
        Init();
    }

    public void Init() {
        
        evaluator = new Evaluator(HandleLoggableEvent); 
        instance = this;    
        lettersOnRack = new List<TileInstance>();
        
        StartCoroutine(LoadGameData());
    }

    IEnumerator LoadGameData() {
        string validWordListFilename = "CSW15.txt";
        string letterFrequencyFilename = "popular_freq.txt";
        string spriteAtlasFilename = "light_gray_atlas.png";
        string mapPlaylistFilename = "map_playlist.txt";

        fileHandler.LoadFile(validWordListFilename, Wordlist.CreateWordList);

        yield return new WaitUntil(() => Wordlist.isLoadingList == false);

        tileGenerator.RegisterTileDrawObserver(HandleTileDraw);

        fileHandler.LoadFile(letterFrequencyFilename, tileGenerator.CreateLetterFrequencyList);
 
        CellSpriteManager.instance.LoadSpriteAtlas(spriteAtlasFilename, fileHandler);

        yield return new WaitUntil(() => CellSpriteManager.instance.isLoadingSprites == false && CellSpriteManager.instance.cellSwatchMap.Count > 0);

        mapGenerator.Init(boardContainer.transform, mapPlaylistFilename);
        mapGenerator.RegisterLastMapCompleteObserver(HandleLastMapCompletedEvent);
        mapGenerator.RegisterLogMessageObserver(HandleLoggableEvent);
        mapGenerator.RegisterMapLoadedObserver(HandleMapLoaded);
        mapGenerator.RegisterMapLoadFailureObserver(HandleMapLoadFailure);
        
        yield return new WaitUntil(() => mapGenerator.mapPlaylist.Count > 0);

        StartGame();

    }

    public void StartGame() {
        turns = new List<Turn>();

        uiManager.Init();
        uiManager.UpdateLegendPanel();
        uiManager.SetHighScore(APIGateway.profile.highScore);
        
        // Create a player
        int startingHealth = 20;

        player = new Player(startingHealth, HandleLoggableEvent);
        player.RegisterScoreChangedObserver(uiManager.SetScore);
        player.RegisterHighScoreChangedObserver(uiManager.SetHighScore);
        player.RegisterHealthZeroedObserver(GameOver);
      
        discardHandler = new DiscardHandler(player, uiManager.healthMeter, HandleLoggableEvent);
        discardHandler.RegisterTileDiscardObserver(HandleTileDiscard);

        randomSeed = Random.Range(0, int.MaxValue);
        Random.InitState(randomSeed);

        // Log random seed
        HandleLoggableEvent($"Seed # {randomSeed}", MessageType.Normal);

        // Create a map    
        mapGenerator.LoadFirstMap();
        
        for (int i = lettersOnRack.Count - 1; i >= 0; i--) {
            TileInstance tile = lettersOnRack[i];
            if (tile != null) {
                tile.Destroy();
            }
        }
        lettersOnRack.Clear();

        tileGenerator.ResetLetterPool();
        // Give player some tiles; can set starting tiles, e.g. "NOTHING", or pass an integer
        tileGenerator.DrawTiles(7);

        uiManager.RegisterSubmitButtonObserver(SubmitPlacedTilesForScoring);

        // Creates a new turn, which stores info about actions player takes
        StartNewTurn();
    }

    public void GameOver()
    {
        string guiMessage = "You destroyed too many valuable tiles. Game over! Thanks for playing.";

        APIGateway.PostData(APIGateway.profile);
        HandleLoggableEvent("Game over!", MessageType.Error);
        mapGenerator.board.Clear();
        uiManager.ShowGameOverScreen(guiMessage);
        player.UnregisterAllObservers();
    }

    void StartNewTurn()
    {
        currentTurn = new Turn();
        turns.Add(currentTurn);
        HandleLoggableEvent($"Starting turn # {turns.Count}", MessageType.Normal);
    }

    void HandleTileDraw(List<TileInstance> tiles)
    {
        string drawnLetters = "";

        foreach (TileInstance t in tiles)
        {
            drawnLetters += t.letter + ",";
            t.transform.SetParent(rackContainer.transform, false);
            int numExistingTiles = rackContainer.transform.childCount;
            t.transform.position = new Vector3(t.transform.position.x + numExistingTiles, t.transform.position.y, t.transform.position.z);
            lettersOnRack.Add(t);
        }
        // remove comma after the last letter in drawn letters
        drawnLetters = drawnLetters.Remove(drawnLetters.Length - 1, 1);

        HandleLoggableEvent($"You drew {tiles.Count} tiles: {drawnLetters}", MessageType.Normal);

        uiManager.UpdateTileCounter(tileGenerator.letterPool.Count);
    }

    void HandleTileDiscard(TileInstance tile) {
        lettersOnRack.Remove(tile);
        tileGenerator.DrawTiles(1);
        soundManager.PlaySFX(SFX.DestroyTile);
    }
    
    void SubmitPlacedTilesForScoring()
    {
        Tuple<int,int> result = evaluator.EvaluatePlacedTiles(mapGenerator.board, currentTurn.placedTileData);

        if (result.Item1 == 0)
        {
            // Some error occurred, probably an invalid word or placement.
            return;
        }

        if (result.Item1 > 0) {
            HandleLoggableEvent($"Score increased by {result.Item1} points!", MessageType.Success);
        }
        if (result.Item2 > 0) {
            HandleLoggableEvent($"Health increased by {result.Item2} points!", MessageType.Success);
        }

    //    player.ModifyHealth(-1); // turn damage

        player.ModifyScore(result.Item1);
        player.ModifyHealth(result.Item2);

        if (player.currentHealth <= 0) {
            return;
        }

        bool hasExited = false;

        foreach (TileData tile in currentTurn.placedTileData)
        {
            TileInstance t = mapGenerator.board.Get(tile.x, tile.y, LayerType.Tile) as TileInstance;
            if (t == null) {
                Debug.LogWarning("No tile instance associated with the placed tile data!");
                continue;
            }
            t.CommitToBoard();
            lettersOnRack.Remove(t);

            if (tile.placedOnCellType == CellType.Exit)
            {
                hasExited = true;
            }
        }

        if (hasExited)
        {
            HandleMapExited();
        }

        tileGenerator.DrawTiles(currentTurn.placedTileData.Count);

        StartNewTurn();
    }

    private void HandleMapExited() {
        for (int i = placedTileContainer.transform.childCount - 1; i >= 0; i--)
        {
            TileInstance tile = placedTileContainer.transform.GetChild(i).GetComponent<TileInstance>();
            tileGenerator.AddLetterToPool(tile.letter, 1);
            uiManager.UpdateTileCounter(tileGenerator.letterPool.Count);
            Destroy(tile.gameObject);
        }
 
        mapGenerator.LoadNextMap();
        soundManager.PlaySFX(SFX.ExitLevel);
    }

    private void HandleMapLoaded() {
        uiManager.SetLevelHeading("Level " + mapGenerator.currentMapNumber);
    }

    private void HandleMapLoadFailure() {
        string message = "You appear to have gotten lost in the maze. Game over!";
        uiManager.ShowGameOverScreen(message);
    }

    public void AddTileToBoard(TileInstance tile)
    {
        mapGenerator.board.Add(tile);
        tile.transform.SetParent(placedTileContainer.transform);

        currentTurn.AddTilePlacement(tile.data);
    }

    public void RemoveTileFromBoard(TileInstance tile)
    {
        if (mapGenerator == null) return;
        if (mapGenerator.board == null) return;
        mapGenerator.board.Remove(tile);

        currentTurn.RemoveTilePlacement(tile.data);
    }
}
