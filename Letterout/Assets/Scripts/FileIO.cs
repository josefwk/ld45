using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine.Networking;
using System;

public class FileIO : MonoBehaviour
{
    public static FileIO instance;
    string serverDirectory = "https://josefwk.com/static/games/Letterout/assets/";

    void Awake()
    {
        instance = this;
    }

    public string LoadFileBytestream(string filename, Action<byte[]> OnDataLoadedHandler) {
        if (filename == null) {
            OnDataLoadedHandler?.Invoke(null);
            return null;
        }

        string serverPath = Path.Combine(serverDirectory, filename);
        StartCoroutine(GetFileBytestreamFromServer(serverPath, OnDataLoadedHandler));
        return serverPath;
    }

    IEnumerator GetFileBytestreamFromServer(string filePath, Action<byte[]> OnDataLoadedHandler)
    {
        UnityWebRequest www = UnityWebRequest.Get(filePath);
    //    www.downloadHandler = new DownloadHandlerBuffer();

        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.ConnectionError  || www.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(www.error);
        }
        else
        {
            byte[] result = www.downloadHandler.data;
            OnDataLoadedHandler?.Invoke(result);
        }
    }

    // Returns the filepath of the requested resource, or null on failure
    public string LoadFile(string filename, Action<string> OnDataLoadedHandler) {
        if (filename == null) {
            OnDataLoadedHandler?.Invoke(null);
            return null;
        }

        string serverPath = Path.Combine(serverDirectory, filename);
        StartCoroutine(GetFileFromServer(serverPath, OnDataLoadedHandler));
        return serverPath;

    /*
       if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            string serverPath = Path.Combine(serverDirectory, filename);
            StartCoroutine(GetFileFromServer(serverPath, OnDataLoadedHandler));
            return serverPath;
        }
        else {
            string localPath = Path.Combine(Application.streamingAssetsPath, filename);
            GetFileFromLocalDisk(localPath, OnDataLoadedHandler);
            return localPath;
        }
    */
    }

    public void Save(string rawTextData, string filename)
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            string serverPath = Path.Combine(serverDirectory, filename);
            StartCoroutine(GetFileFromServer(serverPath, null));
        }
        else
        {
            string localPath = Path.Combine(Application.streamingAssetsPath, filename);
            SaveFileToLocalDisk(rawTextData, localPath);

        }
    }

    IEnumerator GetFileFromServer(string filePath, Action<string> OnDataLoadedHandler)
    {
        UnityWebRequest www = new UnityWebRequest(filePath);
        www.downloadHandler = new DownloadHandlerBuffer();

        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.ConnectionError  || www.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(www.error);
        }
        else
        {
            string textResult = www.downloadHandler.text;
            OnDataLoadedHandler?.Invoke(textResult);
        }
    }

    void GetFileFromLocalDisk(string filePath, Action<string> OnDataLoadedHandler)
    {
        if (File.Exists(filePath))
        {
            string rawText = File.ReadAllText(filePath);
            OnDataLoadedHandler?.Invoke(rawText);
        }
        else
        {
            Debug.LogErrorFormat("Cannot load game data at {0}!", filePath);
        }
    }

    void SaveFileToLocalDisk(string rawTextData, string filePath)
    {
        try
        {
            File.WriteAllText(filePath, rawTextData);
            Debug.LogFormat("Done saving! File data length: {0}", rawTextData.Length);
        }
        catch (IOException e)
        {
            Debug.LogErrorFormat("Cannot save game map data! Destination: {0}", filePath);
            Debug.LogError(e.Message);
            return;
        }
    }
}
