﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DiscardHandler : IDraggableTarget
{
    DiscardMeterView discardMeter;
    IDamageable damageable;
    Action<string, MessageType> loggableEventHandler;
    Action<TileInstance> onTileDiscarded;

    public DiscardHandler(IDamageable damageableInstanceToTrack, DiscardMeterView view, Action<string, MessageType> logHandler) {
        this.damageable = damageableInstanceToTrack;
        this.discardMeter = view;
        this.loggableEventHandler = logHandler;
        discardMeter.SetHealthDisplay(damageable.currentHealth, damageable.maxHealth);

        this.damageable.RegisterHealthChangedObserver(discardMeter.SetHealthDisplay);
    }

    public void RegisterTileDiscardObserver(Action<TileInstance> observer) {
        onTileDiscarded += observer;
    }

    public void UnregisterTileDiscardObserver(Action<TileInstance> observer) {
        onTileDiscarded -= observer;
    }

    public void DiscardTile(TileInstance tile)
    {
        loggableEventHandler?.Invoke($"Destroyed the {tile.letter} tile!", MessageType.Error);
        APIGateway.profile.tilesDestroyed++;
        AchievementObserver.ObserveTileDestroyedEvent(APIGateway.profile.tilesDestroyed);
        damageable.ModifyHealth(-tile.pointValue);
 
        if (damageable.currentHealth > 0)
        {
            onTileDiscarded?.Invoke(tile);
        }

        tile.Destroy();
    }

    void OnDestroy() {
        onTileDiscarded = null;
        this.damageable.UnregisterHealthChangedObserver(discardMeter.SetHealthDisplay);
    }
}
