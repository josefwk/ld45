using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnApplicationStart : MonoBehaviour
{

    bool isPlayerNameLoaded = false;
    bool isPlayerDataLoaded = false;
    bool isAchievementDataLoaded = false;

    void Start()
    {
        APIGateway.OnPlayerNameUpdated += (result) => {this.isPlayerNameLoaded = true; };
        APIGateway.OnPlayerDataUpdated += (result) => {this.isPlayerDataLoaded = true; };
        APIGateway.OnAchievementDataUpdated += () => {this.isAchievementDataLoaded = true; };

        APIGateway.GetPlayerName();
        APIGateway.GetData();
        APIGateway.GetAchievements();
    //    APIGateway.GetItemData();

    }

    void Update() {
        if (!isPlayerNameLoaded || !isPlayerDataLoaded || !isAchievementDataLoaded) return;

        SceneManager.LoadScene("Gameplay");
    }

    void OnDestroy() {
        APIGateway.OnPlayerNameUpdated = null;
        APIGateway.OnPlayerDataUpdated = null;
        APIGateway.OnAchievementDataUpdated = null;
    }
}
