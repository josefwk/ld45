using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour
{
    Vector3 positionOrigin;
    TileInstance lastSelectedTile;

    public Action<int> OnPaletteCellClicked;
    public Action<int, int> OnMapCellClicked;

    int tileZLayer = -1;

    CellInstance2D originCell;

    void Start() {

    }

    public void BeginMouseDrag()
    {
        lastSelectedTile = null;
        originCell = null;

        originCell = GetRaycastTarget<CellInstance2D>();
        TileInstance t = GetRaycastTarget<TileInstance>();

        if (originCell) {
            if (SceneManager.GetActiveScene().name == "MapEditor") {
                if (originCell.transform.parent.name == "PaletteLayer2D") {
                    OnPaletteCellClicked?.Invoke(originCell.id);
                }
                else {
                    OnMapCellClicked?.Invoke(originCell.x, originCell.y);
                }
            }
        }

        if (t) {
            lastSelectedTile = t;
            positionOrigin = t.transform.position;
        }
    }

    void Update()
    {
        Vector3 mouseScreenPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

         // Start mouse drag
        if (Input.GetMouseButtonDown(0))
        {
            BeginMouseDrag();
        }

        // Current mouse drag
        if (Input.GetMouseButton(0) && lastSelectedTile != null)
        {
            if (lastSelectedTile.x == mouseScreenPos.x && lastSelectedTile.y == mouseScreenPos.y) return;

            lastSelectedTile.transform.position = new Vector3(mouseScreenPos.x, mouseScreenPos.y, tileZLayer);
        }

        // Map editor mouse drag
        if (Input.GetMouseButton(0) && SceneManager.GetActiveScene().name == "MapEditor") {
            CellInstance2D c = GetRaycastTarget<CellInstance2D>();

            // differentiating from the palette cells that appear in the map editor
            if (c && c.transform.parent.name == "GameBoardLayer2D") {
                OnMapCellClicked?.Invoke(c.x, c.y);
            }
        }

        // End mouse drag
        if (Input.GetMouseButtonUp(0))
        {
            if (lastSelectedTile == null) return;
            EndMouseDrag(originCell);
        }
    }

    void EndMouseDrag(CellInstance2D origin)
    {
        TileInstance tileEnd = null;
        CellInstance2D destination = GetRaycastTarget<CellInstance2D>();
        DiscardArea discard = GetRaycastTarget<DiscardArea>();
   
        if (discard) {
            GameManager.instance.RemoveTileFromBoard(lastSelectedTile); // could be coming from the board rather than the rack
            GameManager.instance.discardHandler.DiscardTile(lastSelectedTile);
            return;
        }

        if (destination != null) {
            tileEnd = GameManager.instance.mapGenerator.board.Get(destination.x, destination.y, LayerType.Tile) as TileInstance;
        }

        // regardless of origin, destination cell is not valid (either marked as not for placement or has a tile already)
        if (destination != null && (destination.cellType == CellType.Blocked || tileEnd != null)) {
            lastSelectedTile.transform.position = positionOrigin;
        }

        // was not on the board and is still not on the board
        else if (origin == null && destination == null)
        {
            lastSelectedTile.transform.position = positionOrigin;
            lastSelectedTile.transform.SetParent(GameManager.instance.rackContainer.transform, false);
        }

        // was on the board, but is not now
        else if (origin != null && destination == null)
        {
            TileInstance prev = GameManager.instance.mapGenerator.board.Get(origin.x, origin.y, LayerType.Tile) as TileInstance;
            if (prev != null) {
                GameManager.instance.RemoveTileFromBoard(prev);
            }
            lastSelectedTile.transform.position = positionOrigin;
            lastSelectedTile.transform.SetParent(GameManager.instance.rackContainer.transform, false);
        }
        // was on another cell prior to being moved to another valid cell (origin != null && destination != null) 
        // or was not previously on a cell but is now (origin == null && destination != null)
        else {
            if (origin != null) {
                TileInstance prev = GameManager.instance.mapGenerator.board.Get(origin.x, origin.y, LayerType.Tile) as TileInstance;
                if (prev != null) {
                    GameManager.instance.RemoveTileFromBoard(prev);
                }
            }

            Vector3 targetPos = new Vector3(destination.transform.position.x, destination.transform.position.y, tileZLayer);

            lastSelectedTile.data.x = destination.x;
            lastSelectedTile.data.y = destination.y;
            lastSelectedTile.data.placedOnCellType = destination.cellType;
            lastSelectedTile.transform.position = targetPos;
            GameManager.instance.AddTileToBoard(lastSelectedTile);
        }
    }
    
    // raycast against all targets at mouse position and return the first instance of the given type if any are found
    T GetRaycastTarget<T>() {

        Vector2 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Ray ray = new Ray(worldPos, Camera.main.transform.forward);
        RaycastHit2D[] hitInfo = Physics2D.RaycastAll(worldPos, Camera.main.transform.forward);

        foreach (RaycastHit2D hit in hitInfo) {
            if (hit.collider == null) continue;

            T target = hit.collider.GetComponent<T>();
            if (target != null) {
                return target;
            }
        }

        return default(T);
    }
}
