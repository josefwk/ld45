﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DraggableTarget : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    [SerializeField]
    Image cellHighlight;

    static Color32 highlightColor = new Color(255, 128, 0, 255);

    public void OnPointerEnter(PointerEventData eventData)
    {
        SetHighlight(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        SetHighlight(false);
    }

    public void SetHighlight(bool isHighlighted)
    {
        if (isHighlighted)
        {
            this.cellHighlight.color = highlightColor;
        }
        else
        {
            this.cellHighlight.color = Color.clear;
        }

    }

    public Vector3 GetCellCenterPosition()
    {
        Vector2 offset = GetComponent<RectTransform>().sizeDelta;
        return new Vector3(transform.position.x + offset.x / 2.0f, transform.position.y - offset.y / 2.0f, 0);
    }
}
