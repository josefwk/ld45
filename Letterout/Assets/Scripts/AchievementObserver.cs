using System;
using System.Collections.Generic;

public class AchievementObserver {

    public static AchievementData data;

    static List<AchievementCondition> conditions = new List<AchievementCondition>();
    
    static Action<int> onScoreEvent;
    static Action<int> onHealthGainEvent;
    static Action<int> onTileDestroyedEvent;
    static Action<int> onGameWinEvent;
    static Action<int> onWordSubmittedEvent;

    public static void ObserveScoreEvent(int score) {
        onScoreEvent?.Invoke(score);
    }

    public static void ObserveHealthGainEvent(int score) {
        onHealthGainEvent?.Invoke(score);
    }

    public static void ObserveTileDestroyedEvent(int score) {
        onTileDestroyedEvent?.Invoke(score);
    }

    public static void ObserveGameWinEvent(int score) {
        onGameWinEvent?.Invoke(score);
    }

    public static void ObserveWordSubmittedEvent(int score) {
        onWordSubmittedEvent?.Invoke(score);
    }
       
    public static void RegisterAchievementConditions(List<AchievementCondition> conditions) {

        for (int i = 0; i < conditions.Count; i++) {
            AchievementCondition c = conditions[i];
            int achievementId = i;
            switch (c.behavior) {
                
                case AchievementBehavior.Score:
                    onScoreEvent += (amount) => HandleValueUpdate(amount, c as AchievementNumericCondition, achievementId);
                break;
                case AchievementBehavior.HealthGain:
                    onHealthGainEvent += (amount) => HandleValueUpdate(amount, c as AchievementNumericCondition, achievementId);
                break;
                case AchievementBehavior.TileDestroyed:
                    onTileDestroyedEvent += (amount) => HandleValueUpdate(amount, c as AchievementNumericCondition, achievementId);
                break;
                case AchievementBehavior.GameWin:
                    onGameWinEvent += (amount) => HandleValueUpdate(amount, c as AchievementNumericCondition, achievementId);
                break;
                case AchievementBehavior.LongWord:
                    onWordSubmittedEvent += (amount) => HandleValueUpdate(amount, c as AchievementNumericCondition, achievementId);
                break;
            }
        }
    }

    public static void HandleValueUpdate(int playerScore, AchievementNumericCondition a, int achievementId) {
        if (data == null || data.achievements == null) return;
        EvaluateNumericCondition(a.targetNumber, playerScore, achievementId);
    }

    // abstractly represents any generic (stat >= target) situation
    static void EvaluateNumericCondition(int targetAmount, int playerValue, int achievementId) {
        if (playerValue >= targetAmount && !data.achievements[achievementId].achieved) {
            EmitAchievementUnlock(achievementId);
        }
    }

    static void EmitAchievementUnlock(int achievementId) {
        data.achievements[achievementId].achieved = true;
        APIGateway.PostAchievementEarned(achievementId);
    }

}