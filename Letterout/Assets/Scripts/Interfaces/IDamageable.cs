using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface IDamageable
{
    int maxHealth { get; }
    int currentHealth { get; }

    public void ModifyHealth(int amount);
    public void RegisterHealthChangedObserver(Action<int,int> callback);
    public void UnregisterHealthChangedObserver(Action<int,int> callback);
}
