﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LegendPanel : MonoBehaviour
{
    public Image startCell;
    public Image stairCell;
    public Image dlCell;
    public Image dwCell;
    public Image tlCell;
    public Image twCell;
    public Image qlCell;
    public Image qwCell;
    
    void Start()
    {

    }
   
    public void SetLegendPanel() {
        StartCoroutine(GetLegendSprite(startCell, 1));
        StartCoroutine(GetLegendSprite(stairCell, 26));
        StartCoroutine(GetLegendSprite(dlCell, 2));
        StartCoroutine(GetLegendSprite(dwCell, 3));
        StartCoroutine(GetLegendSprite(tlCell, 4));
        StartCoroutine(GetLegendSprite(twCell, 5));
        StartCoroutine(GetLegendSprite(qlCell, 6));
        StartCoroutine(GetLegendSprite(qwCell, 7));
    }

    IEnumerator GetLegendSprite(Image image, int id) {
        yield return new WaitUntil(() => CellSpriteManager.instance.GetSpriteForCellID(id) != null);
        image.sprite = CellSpriteManager.instance.GetSpriteForCellID(id);
    }

}
