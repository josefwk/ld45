﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TileInstance : MonoBehaviour, IInstance2D, IDraggableContainer
{

    public SpriteRenderer spriteBackground;
    public Image imageBackground;
    public TMP_Text letterMesh;
    public TMP_Text valueMesh;

    private TileData _data = new TileData();
    public TileData data {
        get {
            return _data;
        } set {
            _data = value;
        } 
    }
    public int x {
        get
        {
            return data.x;
        }
        set
        {
            data.x = value;
        }
    }

    public int y
    {
        get
        {
            return data.y;
        }
        set
        {
            data.y = value;
        }
    }

    public int id {
        get {
            return ((int)data.letter) - 'A';
        }
        set {
            int charValue = (int) 'A' + value;
            this.letter = (char) charValue;
        }
    }

    public char letter
    {
        get
        {
            return data.letter;
        }
        set
        {
            data.letter = value;

            letterMesh.text = letter.ToString();
            valueMesh.text = pointValue.ToString();
        }
    }
    public int pointValue { 
        get {
            return data.pointValue;
        } 
        private set {} 
    }

    public LayerType layerType { get; set; }

    public void CommitToBoard()
    {
        data.isCommittedToBoard = true;
        this.GetComponent<BoxCollider2D>().enabled = false;
        if (spriteBackground) spriteBackground.color = Color.gray;
        if (imageBackground) imageBackground.color = Color.gray;
    }

    public void Destroy()
    {
        if (this != null && this.gameObject != null) {
            Destroy(this.gameObject);
        }
    }
}
