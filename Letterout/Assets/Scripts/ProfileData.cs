﻿using UnityEngine;
using System.Collections;

public class ProfileData {

    private static string playerName = "Unknown";

    public int highScore = 0;
    public int tilesDestroyed = 0;
    public int wins = 0;
    public int healthGained = 0;
    
    public void SetPlayerName (string name) {
        playerName = name;
    }

    public string GetPlayerName () {
        return playerName;
    }

    public ProfileData() {

    }
}