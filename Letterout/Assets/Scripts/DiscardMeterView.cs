﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DiscardMeterView : MonoBehaviour
{
    public TextMeshProUGUI healthDisplay;

    public RectTransform fillBar;
    public RectTransform fillContainer;


    public void SetHealthDisplay(int remaining, int max)
    {
        healthDisplay.text = remaining.ToString() + " / " + max.ToString();

        float meterWidth = (fillContainer.sizeDelta.x / max) * (remaining);

        fillBar.sizeDelta = new Vector2(meterWidth, fillBar.sizeDelta.y);
    }

}
