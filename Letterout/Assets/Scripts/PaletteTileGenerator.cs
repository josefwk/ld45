﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteTileGenerator : MonoBehaviour
{
    public GameObject paletteContainer;
    public GameObject paletteCellPrefab;
    
    public void Awake() {
       
    }

    public void LoadPalette() {
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                int cellID = y + (x * 8);
                GameObject cellObj = Instantiate(paletteCellPrefab);
                cellObj.GetComponent<CellInstance2D>().id = cellID;
                cellObj.name = cellID.ToString();
                cellObj.transform.SetParent(paletteContainer.transform, false);
            }
        }
    }
}
