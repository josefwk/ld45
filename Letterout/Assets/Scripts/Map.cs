﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Map
{
    public string mapName;
    public int width;
    public int height;
    public List<CellData> cellData;
}
