﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreView : MonoBehaviour
{
    public TextMeshProUGUI score;

    void Start()
    {
        score.text = "0";
    }

    public void SetScore(int value)
    {
        score.text = value.ToString();
    }
}
