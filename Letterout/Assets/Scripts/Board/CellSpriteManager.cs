﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CellSpriteManager : MonoBehaviour
{

    public Dictionary<int, Sprite> cellSwatchMap { get; private set; } 

    public static CellSpriteManager instance;
    public FilterMode filterMode;
    public bool isLoadingSprites = false;

    private void Awake()
    {
        instance = this;
        cellSwatchMap = new Dictionary<int, Sprite>();
    }

    void Start() {

    }

    public void LoadSpriteAtlas(string filename, FileIO fileHandler) {

        isLoadingSprites = true;
        string path = filename;
        int width = 8;
        int height = 8;
        int cellSize = 128;

        Texture2D texture = new Texture2D(width * cellSize, height * cellSize);
        texture.filterMode = this.filterMode;

        fileHandler.LoadFileBytestream(path, (result) => {
            texture.LoadImage(result);
            for (int y = (height - 1), i = 0; y >= 0; y--, i++) {
                for (int x = 0; x < width; x++) {
                    int cellID = x + (i * height);
                    Sprite sprite = Sprite.Create(texture, new Rect(x * cellSize, y * cellSize, cellSize, cellSize), new Vector2(0.5f, 0.5f));
                    sprite.name = cellID.ToString();
                    this.cellSwatchMap[cellID] = sprite;
                }
            }

        });

        isLoadingSprites = false;
    }

    public Sprite GetSpriteForCellID(int id) 
    {
        if (cellSwatchMap == null) return null;
        if (cellSwatchMap.ContainsKey(id) == false) return null;
        return cellSwatchMap[id];
    }
}
