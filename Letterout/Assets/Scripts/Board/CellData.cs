﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CellData
{
    public int x, y;
    
    public int id;
    public CellType cellType;
    
    public CellData()
    {
     
    }

    public CellData(int x, int y) 
    {
        this.x = x;
        this.y = y;
    }

    public CellData(int x, int y, int id, CellType cellType) 
    : this(x, y) {
        this.id = id;
        this.cellType = cellType;      
    }
}
