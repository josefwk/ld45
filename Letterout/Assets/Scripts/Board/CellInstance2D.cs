using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class CellInstance2D : MonoBehaviour, IInstance2D, IDraggableTarget
{
    static Color32 highlightColor = new Color(255, 128, 0, 255);

    [SerializeField]
    SpriteRenderer cellFrame;

    [SerializeField]
    SpriteRenderer cellBonus;

    [SerializeField]
    SpriteRenderer cellBackground;

    [SerializeField]
    SpriteRenderer cellHighlight;

    [SerializeField]
    TMP_Text label;

    public Color32 frameColor;

    private CellData _data = new CellData();
    public CellData data {
        get {
            return _data;
        } 
        set {
            _data = value;

            this.x = data.x;
            this.y = data.y;
            OnCellIDChanged(id);
        }
    }

    public int id {
        get
        {
            return data.id;
        }
        set
        {
            data.id = value;
            OnCellIDChanged(id);
        }
    }

    public int x {
        get
        {
            return data.x;
        }
        set
        {
            data.x = value;
        }
    }

    public int y
    {
        get
        {
            return data.y;
        }
        set
        {
            data.y = value;
        }
    }

  
    public CellType cellType
    {
        get
        {
            return data.cellType;
        }
        set
        {
            data.cellType = value;
        //    OnPlacementTypeChanged(data.cellType);
        }
    }

    public LayerType layerType { get; set; }

    void OnCellIDChanged(int id) {
        cellBackground.sprite = CellSpriteManager.instance.GetSpriteForCellID(data.id);

        if (cellBackground.sprite == null)
        {
            cellBackground.color = Color.clear;
        }
        else
        {
            cellBackground.color = Color.white;
        }

        switch (id) {
            case 1:
                cellType = CellType.Start;
                break;
            case 2:
                cellType = CellType.DoubleLetter;
                break;
            case 3:
                cellType = CellType.DoubleScore;
                break;
            case 4:
                cellType = CellType.TripleLetter;
                break;
            case 5:
                cellType = CellType.TripleScore;
                break;
            case 6:
                cellType = CellType.QuadLetter;
                break;
            case 7:
                cellType = CellType.QuadScore;
                break;
            case 8:
                cellType = CellType.HealthRegen;
                break;
            case 9:
                cellType = CellType.SpecificLetter;
                break;
            case 25:
                cellType = CellType.Basic;
                break;
            case 26:
                cellType = CellType.Exit;
                break;
            default:
                cellType = CellType.Blocked;
                break;
        }

        if (data.cellType == CellType.Blocked) 
        {
            cellFrame.color = Color.clear;
        }
        else 
        {
            cellFrame.color = this.frameColor;
        }

        SetPlacementLabel(data.cellType);
    }

    void SetPlacementLabel(CellType pType)
    {
        this.label.color = new Color32(255, 255, 255, 255);
        this.label.fontSize = 42;
        this.label.alignment = TextAlignmentOptions.Center;

        // set label content
        switch (pType)
        {
            case CellType.DoubleScore:
                label.text = "DW";
                break;
            case CellType.DoubleLetter:
                label.text = "DL";
                break;
            case CellType.TripleScore:
                label.text = "TW";
                break;
            case CellType.TripleLetter:
                label.text = "TL";
                break;
            case CellType.QuadScore:
                label.text = "QW";
                break;
            case CellType.QuadLetter:
                label.text = "QL";
                break;
            default:
                label.text = "";
                break;
        }
    }

    public void Destroy() {
        Destroy(this.gameObject);
    }

}
