using System;

public interface IInstance2D
{
    int x {get; set;}
    int y {get; set;}
    int id {get; set; }
    LayerType layerType {get; set;}
    
    void Destroy();
}
