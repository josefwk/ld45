﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class Board
{

    public int width { get; private set; }
    public int height { get; private set; }

    public Dictionary<LayerType, IInstance2D[,]> layers { get; private set; }

    public Board(int width, int height) {
        layers = new Dictionary<LayerType, IInstance2D[,]>();
        CreateNewBoard(width, height);
    }
    
    public void Clear()
    {
        if (layers == null) return;
        foreach (KeyValuePair<LayerType, IInstance2D[,]> pair in layers) {
            foreach (IInstance2D instance in pair.Value) {    
                if (instance == null) continue;
                instance.Destroy();
            }
        }
        layers.Clear();
        
    }

    public void CreateNewBoard(int width, int height)
    {
        Clear();
        this.width = width;
        this.height = height;
    }

    public void Add(IInstance2D instance)
    {
        LayerType t = instance.layerType;
        TileInstance i = instance as TileInstance;
        if (layers.ContainsKey(t) == false) {
            layers.Add(t, new IInstance2D[width, height]);
        }

        layers[t][instance.x, instance.y] = instance;
    }

    
    public void Remove(IInstance2D instance)
    {
        // Should not destroy objects, just disassociate
        // Consider the case of moving a tile around the board
        LayerType t = instance.layerType;

        if (layers.ContainsKey(t) == false) {
            return;
        }

        layers[t][instance.x, instance.y] = null;
    }

    string PrintBoardState() {

        string output = "";

        if (layers.ContainsKey(LayerType.Tile) == false) return null;

        IInstance2D[,] layer = layers[LayerType.Tile];
        string layerOutput = "";
        for (int y = height - 1; y >= 0; y--) {
            for (int x = 0; x < width; x++) {
                TileInstance t = layer[x,y] as TileInstance;
                if (t == null) layerOutput += "- ";
                else layerOutput += t.letter + " ";
            }
            layerOutput += "\n";
        }
        Debug.Log(layerOutput);
        output += layerOutput + "\n\n";

        return output;
       
    }

    public IInstance2D Get(int x, int y, LayerType t) {
        if (x < 0 || x >= width || y < 0 || y >= height) return null;
        if (layers.ContainsKey(t) == false) {
            return null;
        }

        return layers[t][x, y];
    }
    
    private Tuple<int, int> GetNeighboringPosition(int x, int y, Direction direction) {
        // origin coordinates are from bottom left
        if (
               (y == 0 && direction == Direction.South)
            || (y == height && direction == Direction.North)
            || (x == 0 && direction == Direction.West)
            || (x == width && direction == Direction.East)
           )
        {
            return null;
        }

        int deltaPosition = 1;
        int deltaX = x;
        int deltaY = y;

        switch (direction)
        {
            case Direction.North:
                deltaY += deltaPosition;
                break;
            case Direction.South:
                deltaY -= deltaPosition;
                break;
            case Direction.West:
                deltaX -= deltaPosition;
                break;
            case Direction.East:
                deltaX += deltaPosition;
                break;
            default:
                break;
        }

        return new Tuple<int, int>(deltaX, deltaY);
    }

    public TileInstance GetNeighboringTile(int x, int y, Direction direction) {
        Tuple<int, int> pos = GetNeighboringPosition(x, y, direction);
        if (pos == null) return null;
        
        IInstance2D instance = Get(pos.Item1, pos.Item2, LayerType.Tile);

        if (instance == null) return null;
        return instance as TileInstance;
    }

    /*  When used as part of the validation step for submitting a word, 
        examine every tile played this turn and for each,
        look in every direction for a connection to another 
        previously scored tile. There must be at least one 
        connection to a previously scored tile.
        If not, the letters are misplaced.
        This assumes the minimum valid word length is 2.
        This also does not consider disconnected words 
        placed on the start tile as valid.
    */
    public bool HasConnectionToAnExistingTile(List<TileData> tiles) {
        bool isConnected = false;

        foreach (TileData tile in tiles)
        {
            foreach (Direction dir in System.Enum.GetValues(typeof(Direction)))
            {
                Tuple<int, int> position = GetNeighboringPosition(tile.x, tile.y, dir);
                if (position == null) continue;

                IInstance2D neighbor = Get(position.Item1, position.Item2, LayerType.Tile);

                if (neighbor == null) continue;
                TileInstance instance = neighbor as TileInstance;

                bool isNeighborATilePlayedThisTurn = tiles.Contains(instance.data);
                if (isNeighborATilePlayedThisTurn == false) isConnected = true;

                if (isConnected) return isConnected;
            }

        }

        return isConnected;
    }

    public bool HasPlacementOnStartTile(List<TileData> tiles)
    {
        bool isOnStartTile = false;

        foreach (TileData tile in tiles)
        {
            isOnStartTile = (tile.placedOnCellType == CellType.Start);

            if (isOnStartTile) return isOnStartTile;
        }
        return isOnStartTile;
    }

    public bool IsTileSetInLine(List<TileData> tiles)
    {
        List<int> xCoords = new List<int>();
        List<int> yCoords = new List<int>();

        foreach (TileData tile in tiles)
        {
            xCoords.Add(tile.x);
            yCoords.Add(tile.y);
        }

        xCoords.Sort();
        yCoords.Sort();

        int xMin = xCoords.Min();
        int yMin = yCoords.Min();

        if (xCoords.Distinct().Count() > 1 && yCoords.Distinct().Count() > 1)
        {
            // not a horizontal or vertical line
            return false;
        }

        return true;
    }

    public bool IsTileSetWithoutGaps(List<TileData> tiles) {

        List<int> xCoords = new List<int>();
        List<int> yCoords = new List<int>();

        foreach (TileData tile in tiles)
        {
            xCoords.Add(tile.x);
            yCoords.Add(tile.y);
        }

        xCoords.Sort();
        yCoords.Sort();

        List<int> xGaps = Enumerable.Range(xCoords.First(),
                            xCoords.Last() - xCoords.First() + 1)
                           .Except(xCoords).ToList();

        List<int> yGaps = Enumerable.Range(yCoords.First(),
                            yCoords.Last() - yCoords.First() + 1)
                           .Except(yCoords).ToList();

        foreach (int gap in xGaps)
        {
            IInstance2D instance = Get(gap, yCoords[0], LayerType.Tile);
            if (instance == null) return false;
        }

        foreach (int gap in yGaps)
        {
            IInstance2D instance = Get(xCoords[0], gap, LayerType.Tile);
            if (instance == null) return false;
        }
        return true;
    }

}
