[System.Serializable]
public class Achievement {
    public int id;
    public string title;
    public string description;
    public string lockedDescription;
    public string icon;
    public string lockedIcon;
    public bool achieved;
}

[System.Serializable]
public class AchievementData {
    public Achievement[] achievements;

    public AchievementData() {

    }
}


public class AchievementCondition {
    public AchievementBehavior behavior;

    public AchievementCondition(AchievementBehavior behavior) {
        this.behavior = behavior;
    }
}

public class AchievementNumericCondition : AchievementCondition {
    public int targetNumber;

    public AchievementNumericCondition(AchievementBehavior behavior, int targetNumber) : base(behavior) {
        this.targetNumber = targetNumber;
    }
}


public class AchievementEnumNumericCondition<T> : AchievementNumericCondition {
    public T targetEnum;

    public AchievementEnumNumericCondition(AchievementBehavior behavior, int targetNumber, T enumeration) : base(behavior, targetNumber) {
        this.targetEnum = enumeration;
    }
}

public enum AchievementBehavior {
    HealthGain,
    TileDestroyed,
    Score,
    LongWord,
    GameWin
}