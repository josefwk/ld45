using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AOT;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

public class APIGateway : MonoBehaviour
{

    APIGateway self;

    public static Action<string> OnPlayerNameUpdated;
    public static Action<ProfileData> OnPlayerDataUpdated;
    public static Action OnAchievementDataUpdated;

    public static ProfileData profile;

    [DllImport("__Internal")]
    public static extern void WebGLPostData(string playerDataJson);

    [DllImport("__Internal")]
    public static extern void WebGLPostPlayerHighScore(int score);

    [DllImport("__Internal")]
    public static extern void WebGLPostAchievementEarned(int achievementId);

    [DllImport("__Internal")]
    public static extern void WebGLGetPlayerName(Action<string> callback);

    [DllImport("__Internal")]
    public static extern void WebGLGetData(Action<string> callback);

    [DllImport("__Internal")]
    public static extern void WebGLGetAchievements(Action<string> callback);

    [DllImport("__Internal")]
    public static extern void WebGLGetItemData(Action<string> callback);

    void Awake()
    {
        if (self != null && self != this) {
            Destroy(this.gameObject);
            return;
        }
        else {
            this.transform.parent = null;
            self = this;

            profile = new ProfileData();
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start() {
        List<AchievementCondition> conditions = new List<AchievementCondition>();
        
        conditions.Add(new AchievementNumericCondition(AchievementBehavior.Score, 100));
        conditions.Add(new AchievementNumericCondition(AchievementBehavior.Score, 1000));
        conditions.Add(new AchievementNumericCondition(AchievementBehavior.HealthGain, 10));
        conditions.Add(new AchievementNumericCondition(AchievementBehavior.HealthGain, 100));
        conditions.Add(new AchievementNumericCondition(AchievementBehavior.TileDestroyed, 10));
        conditions.Add(new AchievementNumericCondition(AchievementBehavior.TileDestroyed, 100));
        conditions.Add(new AchievementNumericCondition(AchievementBehavior.GameWin, 1));
        conditions.Add(new AchievementNumericCondition(AchievementBehavior.GameWin, 5));
        conditions.Add(new AchievementNumericCondition(AchievementBehavior.LongWord, 7));
        conditions.Add(new AchievementNumericCondition(AchievementBehavior.LongWord, 10));

        AchievementObserver.RegisterAchievementConditions(conditions);
    }

    public static void PostData(ProfileData p) {
        string json = JsonConvert.SerializeObject(p);
        #if UNITY_EDITOR
            Debug.LogFormat("Posting data JSON: {0}", json);
        #else 
            WebGLPostData(json);
        #endif
    }
    
    public static void PostPlayerHighScore(int score) {
        #if UNITY_EDITOR
            Debug.LogFormat("Posting high score as: {0}", score);
        #else 
            WebGLPostPlayerHighScore(score);
        #endif
    }

    public static void PostAchievementEarned(int achievementId) {
        #if UNITY_EDITOR
            Debug.LogFormat("Posting achievement earned: {0}", achievementId);
        #else 
            WebGLPostAchievementEarned(achievementId);
        #endif
    }

    public static void GetPlayerName() {
        #if UNITY_EDITOR
            Debug.LogFormat("Getting player name...");
            SetPlayerName("Mock");
        #else 
            WebGLGetPlayerName(SetPlayerName);
        #endif
    }

    public static void GetData() {
        #if UNITY_EDITOR
            Debug.LogFormat("Getting player data...");
            string mock = "{'highScore':48,'tilesDestroyed':8,'wins':0,'healthGained':9}";
            SetData(mock); 
        #else 
            WebGLGetData(SetData);
        #endif
    }

    public static void GetAchievements() {
        #if UNITY_EDITOR
            Debug.LogFormat("Getting player achievements...");
            string mock = @"
            {""achievements"":[
                {""achievementId"":0,""achieved"":false},
                {""achievementId"":1,""achieved"":false},
                {""achievementId"":2,""achieved"":false},
                {""achievementId"":3,""achieved"":false},
                {""achievementId"":4,""achieved"":false},
                {""achievementId"":5,""achieved"":false},
                {""achievementId"":6,""achieved"":false},
                {""achievementId"":7,""achieved"":false},
                {""achievementId"":8,""achieved"":false},
                {""achievementId"":9,""achieved"":false},
            ],
            ""playerData"":[]}"; 
            SetAchievementData(mock);
        #else 
            WebGLGetAchievements(SetAchievementData);
        #endif
    }

     public static void GetItemData() {
        #if UNITY_EDITOR
            Debug.LogFormat("Getting player item data...");
            SetItemData("[]");
        #else 
            WebGLGetItemData(SetItemData);
        #endif
    }

    [MonoPInvokeCallback(typeof(Action<string>))]
    private static void SetPlayerName (string name) {
        profile.SetPlayerName(name);
        OnPlayerNameUpdated?.Invoke(name);
    }

    [MonoPInvokeCallback(typeof(Action<string>))]
    private static void SetData(string json) {
        if (json != null) {
            ProfileData p = JsonConvert.DeserializeObject<ProfileData>(json);
            p.SetPlayerName(profile.GetPlayerName()); // since name is not passed in with data, copy data to avoid overwriting it
            profile = p;
        }

        OnPlayerDataUpdated?.Invoke(profile);
    }

    [MonoPInvokeCallback(typeof(Action<string>))]
    private static void SetAchievementData(string achievementJson) {
        if (achievementJson != null) {
            AchievementData achievementData = JsonConvert.DeserializeObject<AchievementData>(achievementJson);
            AchievementObserver.data = achievementData;
        }
        
        OnAchievementDataUpdated?.Invoke();
    }

    [MonoPInvokeCallback(typeof(Action<string>))]
    private static void SetItemData(string itemDataJson) {
    //    MetalangIO.LoadAbilities(itemDataJson);
    }
}