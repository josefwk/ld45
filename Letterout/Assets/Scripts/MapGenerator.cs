﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class MapGenerator : MonoBehaviour
{
    public GameObject cellPrefab;

    public Board board;
    private Transform boardContainer;

    public List<string> mapPlaylist;
    public int currentMapNumber {get; private set;}

    Action<string, MessageType> loggableEventHandler;
    Action lastMapCompletedHandler;
    Action mapLoadedHandler;
    Action mapLoadFailureHandler;

    float xOffset;
    float yOffset;
    float xSize;
    float ySize;

    #region Observer Registration/Unregistration

    public void RegisterLogMessageObserver(Action<string, MessageType> observer) {
        loggableEventHandler += observer;
    }

    public void RegisterLastMapCompleteObserver(Action observer) {
        lastMapCompletedHandler += observer;
    }

    public void UnregisterLogMessageObserver(Action<string, MessageType> observer) {
        loggableEventHandler -= observer;
    }

    public void UnregisterLastMapCompleteObserver(Action observer) {
        lastMapCompletedHandler -= observer;
    }

    public void RegisterMapLoadedObserver(Action observer) {
        mapLoadedHandler += observer;
    }

    public void UnregisterMapLoadedObserver(Action observer) {
        mapLoadedHandler -= observer;
    }

    public void RegisterMapLoadFailureObserver(Action observer) {
        mapLoadFailureHandler += observer;
    }

    public void UnregisterMapLoadFailureObserver(Action observer) {
        mapLoadFailureHandler -= observer;
    }

    private void UnregisterAllObservers() {
        loggableEventHandler = null;
        lastMapCompletedHandler = null;
        mapLoadedHandler = null;
        mapLoadFailureHandler = null;
    }

    #endregion

    private void Start()
    {
        mapPlaylist = new List<string>();
        RectTransform rt = cellPrefab.GetComponent<RectTransform>();
        Transform t = cellPrefab.GetComponent<Transform>();
        if (rt) {
            xSize = rt.sizeDelta.x;
            ySize = rt.sizeDelta.y;
            xOffset = xSize / 2;
            yOffset = ySize / 2;
        }
        else {
            xSize = t.localScale.x;
            ySize = t.localScale.y;
            xOffset = 0;
            yOffset = 0;
        }

        
    }

    public void Init(Transform boardContainer, string mapPlaylistFileName) {
        this.boardContainer = boardContainer;
        FileIO.instance.LoadFile(mapPlaylistFileName, OnMapPlaylistLoaded);
    }

    public void LoadFirstMap() {
        currentMapNumber = 0;
        LoadNextMap();
    }

    public void LoadNextMap() {
        FileIO.instance.LoadFile(GetNextMapFileName(), LoadMapCallback);
    }

    private void LoadMapCallback(string rawText)
    {   
        if (rawText == null) {
            int mapsRemaining = mapPlaylist.Count;
            if (mapsRemaining != 0) {
                Debug.LogErrorFormat("Failed to load map. Number of maps in queue: {0}", mapsRemaining);
                mapLoadFailureHandler?.Invoke();
            }
            loggableEventHandler?.Invoke("You Win!", MessageType.Success);
            lastMapCompletedHandler?.Invoke();
            return;
        }

        Map map = JsonUtility.FromJson<Map>(rawText);

        loggableEventHandler?.Invoke($"Welcome to level {currentMapNumber}...", MessageType.Normal);

        LoadMap(map, boardContainer);
    }
    
    public void LoadMap(Map map, Transform boardContainer)
    {
        if (board != null) 
            board.Clear();
        
        board = new Board(map.width, map.height);

        foreach (CellData data in map.cellData)
        {
            CreateCell(boardContainer, data);
        }

        mapLoadedHandler?.Invoke();
    }

    private void OnMapPlaylistLoaded(string rawText) {
        string[] mapFileNames = rawText.Trim().Split('\n');
        
        mapPlaylist = new List<string>();
        foreach (string mapFileName in mapFileNames)
        {
            mapPlaylist.Add(mapFileName);
        }

        currentMapNumber = 0;
    }

    private string GetNextMapFileName() {
        if (mapPlaylist.Count == 0 || mapPlaylist.Count <= currentMapNumber) return null;
        
        string mapName = mapPlaylist[currentMapNumber];

        currentMapNumber++;
        return mapName;
    }


    // Generates a generic map data structure
    public void GenerateMap(int mapWidth, int mapHeight, Transform boardContainer)
    {
        board = new Board(mapWidth, mapHeight);

        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                CreateCell(boardContainer, x, y);
            }
        }
    }

    // Creates a cell for the map
    private void CreateCell(Transform container, CellData data)
    {
        GameObject cellObj = Instantiate(cellPrefab, container, false);
        cellObj.name = "Cell (" + data.x + ", " + data.y + ")";

        cellObj.transform.localPosition = new Vector3((data.x * xSize) - xOffset, (data.y * ySize) + xOffset, 0);

        CellInstance2D cell2D = cellObj.GetComponent<CellInstance2D>();

        if (cell2D) {
            cell2D.data = data;
            cell2D.layerType = LayerType.Cell;
            board.Add(cell2D);
        }

    }


    // Creates a cell for the map
    private void CreateCell(Transform container, int x, int y)
    {
        CreateCell(container, new CellData(x, y));
    }

    public void UpdateCell(int x, int y, int cellID) {
        CellInstance2D cell = board.Get(x, y, LayerType.Cell) as CellInstance2D;
        cell.id = cellID;
    }

    private void OnDestroy() {
        UnregisterAllObservers();
    }

}
