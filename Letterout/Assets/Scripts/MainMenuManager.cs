﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuManager : MonoBehaviour {

    public Button startGame;
    public TextMeshProUGUI highScore;
    public TextMeshProUGUI playerName;

	// Use this for initialization
	void Start () {
        startGame.onClick.AddListener(() => this.gameObject.transform.parent.gameObject.SetActive(false));
        APIGateway.OnPlayerNameUpdated += SetPlayerName;

        RequestPlayerName();
        SetHighScore(APIGateway.profile.highScore);
    }

    private void RequestPlayerName () {
        string playerName = APIGateway.profile.GetPlayerName();
        SetPlayerName(playerName);
    }

    private void SetPlayerName (string name) {
        playerName.text = name;
    }

    private void SetHighScore (int score) {
        if (score > 0) {
            highScore.text = score.ToString();
        }
    }

    void OnDestroy() {
        APIGateway.OnPlayerNameUpdated -= SetPlayerName;
    }
}