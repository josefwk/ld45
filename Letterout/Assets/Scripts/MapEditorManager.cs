﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;
using System;
using System.IO;

public class MapEditorManager : MonoBehaviour
{

    public GameObject boardContainer;
    public GameObject paletteRestrictionContainer;

    public Button saveMapButton;
    public Button loadMapButton;
    public TMP_InputField mapNameField;

    public MapGenerator mapGenerator;
    public PaletteTileGenerator paletteGenerator;

    public FileIO fileHandler;

    InputManager inputManager;

    public int selectedCellID;

    private void Start()
    {
        inputManager = GameObject.FindObjectOfType<InputManager>();
        inputManager.OnPaletteCellClicked += UpdatePaletteCellSelection;
        inputManager.OnMapCellClicked += UpdateMapCell;

        StartCoroutine(LoadGameData());

        saveMapButton.onClick.AddListener(() => {
            string mapName = mapNameField.text;
            string rawData = GetMapDataAsJSON(mapName);
            FileIO.instance.Save(rawData, mapName);
        });
        loadMapButton.onClick.AddListener(() => FileIO.instance.LoadFile(mapNameField.text, LoadEditorMap));
    }

    void UpdatePaletteCellSelection(int cellID) {
        selectedCellID = cellID;
    }

    void UpdateMapCell(int x, int y) {
        mapGenerator.UpdateCell(x, y, selectedCellID);
    }

    IEnumerator LoadGameData() {
        CellSpriteManager.instance.LoadSpriteAtlas("light_gray_atlas.png", fileHandler);
        
        yield return new WaitUntil(() => CellSpriteManager.instance.isLoadingSprites == false && CellSpriteManager.instance.cellSwatchMap.Count > 0);
 
        paletteGenerator.LoadPalette();
        
        // Create a map
        mapGenerator.GenerateMap(13, 13, boardContainer.transform);
    }


    void LoadEditorMap(string rawText)
    {
        Map map = JsonUtility.FromJson<Map>(rawText);
        mapGenerator.LoadMap(map, boardContainer.transform);
    }

     public string GetMapDataAsJSON(string mapName)
    {
        Board board = mapGenerator.board;

        Map map = new Map();
        map.mapName = mapName;
        map.width = board.width;
        map.height = board.height;
        map.cellData = new List<CellData>();

        foreach (CellInstance2D cell in board.layers[LayerType.Cell])
        {
            map.cellData.Add(new CellData(cell.x, cell.y, cell.id, cell.cellType));
        }

        string json = JsonUtility.ToJson(map);

        return json;
    }
}
