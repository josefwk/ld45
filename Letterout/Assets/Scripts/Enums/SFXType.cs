﻿
public enum SFX
{
    PutTile,
    GetTile,
    DestroyTile,
    ExitLevel,
    PointGain,
    Error
}