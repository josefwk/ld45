﻿

public enum CellType
{
    Basic,
    Start,
    SpecificLetter,
    SpecificValue,
    Blocked,
    Exit,
    DoubleScore,
    DoubleLetter,
    TripleScore,
    TripleLetter,
    QuadScore,
    QuadLetter,
    HealthRegen
}