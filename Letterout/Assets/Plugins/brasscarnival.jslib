var BrassCarnivalLib = {
    // according to the documentation, IL2CPP will take care of calling _free on string memory
    // TODO: Move all repeated code into a utility function that returns a UTF8 string pointer 
    WebGLPostData : function(playerDataJson) {
        window.SetData(UTF8ToString(playerDataJson));
    },
    
    WebGLPostPlayerHighScore : function(score) {
        window.SetHighScore(score);
    },

    WebGLPostAchievementEarned : function(achievementId) {
        window.SetAchievement(achievementId);
    },

    WebGLGetPlayerName : async function(callback) {
        var result = await window.RequestPlayerName();
        if (result === null || result === undefined) {
            Module.dynCall_vi(callback, null);  
            return;
        }

        var resultLength = lengthBytesUTF8(result) + 1;
        var resultPtr = _malloc(resultLength);
        stringToUTF8(result, resultPtr, resultLength);
        Module.dynCall_vi(callback, resultPtr);

    },

    WebGLGetData: async function(callback) {
        var result = await window.RequestPlayerData();
        if (result === null || result === undefined) {
            Module.dynCall_vi(callback, null);  
            return;
        }
        var resultLength = lengthBytesUTF8(result) + 1;
        var resultPtr = _malloc(resultLength);
        stringToUTF8(result, resultPtr, resultLength);
        Module.dynCall_vi(callback, resultPtr);
    },

    WebGLGetAchievements: async function(callback) {
        var result = await window.RequestGameAndPlayerAchievements();
        if (result === null || result === undefined) {
            Module.dynCall_vi(callback, null);
            return;
        }

        var resultLength = lengthBytesUTF8(result) + 1;
        var resultPtr = _malloc(resultLength);
        stringToUTF8(result, resultPtr, resultLength);
        Module.dynCall_vi(callback, resultPtr);
    },

    WebGLGetItemData: async function(callback) {
        var result = await window.RequestPlayerItemData();
        if (result === null || result === undefined) {
            Module.dynCall_vi(callback, null);  
            return;
        }
        
        var resultLength = lengthBytesUTF8(result) + 1;
        var resultPtr = _malloc(resultLength);
        stringToUTF8(result, resultPtr, resultLength);
        Module.dynCall_vi(callback, resultPtr);
    },

};

mergeInto(LibraryManager.library, BrassCarnivalLib);