using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

public class WordEvaluatorTests
{

    [SetUp]
    public void SetupWordlist() {
        Wordlist.AddWordToList("kale");
        Wordlist.AddWordToList("cheese");
        Wordlist.AddWordToList("chess");
        Wordlist.AddWordToList("ant");
        Wordlist.AddWordToList("dorm");
    }

    [UnityTest]
    public IEnumerator WhenSearchingForWordInList_ExpectTrue()
    {
        // Arrange
        string wordToTest = "ant";
        
        // Act
        bool containsWord = Wordlist.ContainsWord(wordToTest);

        // Assert
        Assert.True(containsWord);
        yield return null;
    }

    [UnityTest]
    public IEnumerator WhenSearchingForWordNotInList_ExpectFalse()
    {
        // Arrange
        string wordToTest = "dormant";

        // Act
        bool containsWord = Wordlist.ContainsWord(wordToTest);

        // Assert
        Assert.False(containsWord);
        yield return null;
    }

    [UnityTest]
    public IEnumerator WhenSearchingForUnusualCaseWordInList_ExpectTrue()
    {
        // Arrange
        string wordToTest = "cHeEsE";

        // Act
        bool containsWord = Wordlist.ContainsWord(wordToTest);

        // Assert
        Assert.True(containsWord);
        yield return null;
    }


    [UnityTest]
    public IEnumerator CanAccessWordlistLoaderInstanceFromPrefabInResourceFolder()
    {
        // Arrange
        var resource = Resources.Load("_FileIOPrefab");
        GameObject obj = resource as GameObject;
        
        // Act
        FileIO instance = obj.GetComponent<FileIO>();

        // Assert
        Assert.NotNull(instance);
	
	    yield return null;
    }

    [UnityTest]
    public IEnumerator WhenWordlistLoaded_ExpectDataStoredInMemory()
    {
        // Arrange
        string testWord = "AX";
        var resource = Resources.Load("_FileIOPrefab");
        GameObject obj1 = Object.Instantiate(resource) as GameObject;
        FileIO loader = obj1.GetComponent<FileIO>();
        loader.LoadFile("CSW15.txt", Wordlist.CreateWordList);

        // Act
        // wait for wordlist to load from file
        yield return new WaitUntil(() => Wordlist.isLoadingList == false);
        bool containsTestWord1 = Wordlist.ContainsWord(testWord);
        bool containsTestWord2 = Wordlist.words.Contains(testWord);

        // Assert
        Assert.True(containsTestWord1);
        Assert.True(containsTestWord2);
        
        yield return null;
    }

    [UnityTest]
    public IEnumerator WhenSingleTileSubmittedForScoringEvaluation_ExpectScoringFailure()
    {
        // Arrange
        char letter = 'A';
        int x = 4, y = 5;
        CellType cellTypeOfFakeBoardLocation = CellType.Start;

        TileData data = new TileData(x, y, letter);
        data.placedOnCellType = cellTypeOfFakeBoardLocation;

        int width = 5;
        int height = 10;
        Board board = new Board(width, height);

        var resource = Resources.Load("2DTile");
        GameObject obj = Object.Instantiate(resource) as GameObject;
        TileInstance instance = obj.GetComponent<TileInstance>();
        instance.data = data;

        board.Add(instance);

        List<TileData> fakeTilesPlayed = new List<TileData>();
        fakeTilesPlayed.Add(instance.data);

        var resource2 = Resources.Load("_FileIOPrefab");
        GameObject obj2 = Object.Instantiate(resource2) as GameObject;
        FileIO loader = obj2.GetComponent<FileIO>();
        loader.LoadFile("CSW15.txt", Wordlist.CreateWordList);

        Evaluator evaluator = new Evaluator();
        int scoreRepresentingFailedAttempt = 0;
        // Act
        // wait for wordlist to load
        yield return new WaitUntil(() => Wordlist.isLoadingList == false);
        Tuple<int,int> result = evaluator.EvaluatePlacedTiles(board, fakeTilesPlayed);
        
        // Assert
        Assert.AreEqual(result.Item1, scoreRepresentingFailedAttempt);

        yield return null;
    }

    [UnityTest]
    public IEnumerator WhenTwoTileWordSubmittedForScoringEvaluation_ExpectScoringSuccess()
    {
        // Arrange
        char letter1 = 'A';
        int x1 = 4, y1 = 6;
        CellType cellTypeOfFakeBoardLocation1 = CellType.Start;

        char letter2 = 'X';
        int x2 = 4, y2 = 5;
        CellType cellTypeOfFakeBoardLocation2 = CellType.Basic;

        TileData data1 = new TileData(x1, y1, letter1);
        data1.placedOnCellType = cellTypeOfFakeBoardLocation1;

        TileData data2 = new TileData(x2, y2, letter2);
        data2.placedOnCellType = cellTypeOfFakeBoardLocation2;

        int width = 5;
        int height = 10;
        Board board = new Board(width, height);

        var resource = Resources.Load("2DTile");
        GameObject obj1 = Object.Instantiate(resource) as GameObject;
        TileInstance instance1 = obj1.GetComponent<TileInstance>();
        instance1.data = data1;
        instance1.layerType = LayerType.Tile;

        GameObject obj2 = Object.Instantiate(resource) as GameObject;
        TileInstance instance2 = obj2.GetComponent<TileInstance>();
        instance2.data = data2;
        instance2.layerType = LayerType.Tile;

        board.Add(instance1);
        board.Add(instance2);

        List<TileData> fakeTilesPlayed = new List<TileData>();
        fakeTilesPlayed.Add(instance1.data);
        fakeTilesPlayed.Add(instance2.data);

        var res = Resources.Load("_FileIOPrefab");
        GameObject obj3 = Object.Instantiate(res) as GameObject;
        FileIO wordlist = obj3.GetComponent<FileIO>();
        wordlist.LoadFile("CSW15.txt", Wordlist.CreateWordList);

        Evaluator evaluator = new Evaluator();
        int scoreRepresentingSuccessfulAttempt = 9; // Word is "AX", A = 1, X = 8

        // Act
        // wait for wordlist to load
        yield return new WaitUntil(() => Wordlist.isLoadingList == false);
         Tuple<int,int> result = evaluator.EvaluatePlacedTiles(board, fakeTilesPlayed);
        
        // Assert
        Assert.AreEqual(scoreRepresentingSuccessfulAttempt, result.Item1);

        yield return null;
    }
}
