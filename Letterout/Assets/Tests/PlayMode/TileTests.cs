using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class TileTests
{
  
    [UnityTest]
    public IEnumerator CanAccessTileInstanceFromPrefabInResourceFolder()
    {
        // Arrange
        var resource = Resources.Load("2DTile");
        GameObject obj = resource as GameObject;
        
        // Act
        TileInstance instance = obj.GetComponent<TileInstance>();

        // Assert
        Assert.NotNull(instance);
	
	    yield return null;
    }

    [UnityTest]
    public IEnumerator WhenCreatingTileData_ExpectPropertiesSet()
    {
        // Arrange
        char letter = 'Q';
        int x = 4, y = 5;
        CellType cellTypeOfFakeBoardLocation = CellType.Start;

        // Act
        TileData data = new TileData(x, y, letter);
        data.placedOnCellType = cellTypeOfFakeBoardLocation;

        // Assert
        Assert.NotNull(data);
        Assert.AreEqual(data.x, x);
        Assert.AreEqual(data.y, y);
        Assert.AreEqual(data.placedOnCellType, cellTypeOfFakeBoardLocation);

        yield return null;
    }

    [UnityTest]
    public IEnumerator WhenAddingTileDataToInstance_ExpectPropertiesSet()
    {
        // Arrange
        char letter = 'Q';
        int x = 4, y = 5;
        CellType cellTypeOfFakeBoardLocation = CellType.Start;

        TileData data = new TileData(x, y, letter);
        data.placedOnCellType = cellTypeOfFakeBoardLocation;
        
        var resource = Resources.Load("2DTile");
        GameObject obj = resource as GameObject;
        TileInstance instance = obj.GetComponent<TileInstance>();
 
        // Act
        instance.data = data;

        // Assert
        Assert.AreEqual(instance.data.x, x);
        Assert.AreEqual(instance.x, x);
        Assert.AreEqual(instance.data.y, y);
        Assert.AreEqual(instance.y, y);
        Assert.AreEqual(instance.letter, letter);
        Assert.AreEqual(instance.data.placedOnCellType, cellTypeOfFakeBoardLocation);
   
	    yield return null;
    }
}
