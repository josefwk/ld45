using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class CellTests
{
    [UnityTest]
    public IEnumerator WhenCellDataConstructedWithCoords_ExpectObject()
    {
        // Arrange
        Vector2Int position = new Vector2Int(3, 5);

        // Act
        CellData data = new CellData(position.x, position.y);

        // Assert
        Assert.NotNull(data);
        Assert.AreEqual(position.x, data.x);
    
        yield return null;
    }

}
