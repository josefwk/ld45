using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class BoardTests
{
   
    [UnityTest]
    public IEnumerator WhenCreatingBoard_ExpectPropertiesSet()
    {
        // Arrange
        int width = 5;
        int height = 10;

        // Act
        Board board = new Board(width, height);

        // Assert
        Assert.NotNull(board);
        Assert.AreEqual(board.width, width);
        Assert.AreEqual(board.height, height);

        yield return null;
    }

    
    [UnityTest]
    public IEnumerator WhenTileDataAddedToBoard_ExpectDataStoredInBoard()
    {
        // Arrange
        char letter = 'A';
        int x = 4, y = 5;
        CellType cellTypeOfFakeBoardLocation = CellType.Start;

        TileData data = new TileData(x, y, letter);
        data.placedOnCellType = cellTypeOfFakeBoardLocation;

        int width = 5;
        int height = 10;
        Board board = new Board(width, height);

        var resource = Resources.Load("2DTile");
        GameObject obj = resource as GameObject;
        TileInstance instance = obj.GetComponent<TileInstance>();
        instance.data = data;
        instance.layerType = LayerType.Tile;

        // Act
        board.Add(instance);
        IInstance2D result = board.Get(x, y, LayerType.Tile);

        // Assert
        Assert.AreEqual(result, instance);

        yield return null;
    }
}
