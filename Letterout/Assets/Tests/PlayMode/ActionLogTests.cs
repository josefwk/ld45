using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class ActionLogTests
{

    [UnityTest]
    public IEnumerator WhenLogMessageIsConstructedParameterless_ExpectNormalMessageType()
    {
        // Arrange
        MessageType mType = MessageType.Normal;

        // Act
        ILogMessage logMessage = new LogMessage();

        // Assert
        Assert.AreEqual(mType, logMessage.messageType);
	
	    yield return null;
    }

    [UnityTest]
    public IEnumerator WhenLogMessageIsConstructed_ExpectNormalMessageType()
    {
        // Arrange
        MessageType mType = MessageType.Normal;
        string testString = "This is a test.";

        // Act
        ILogMessage logMessage = new LogMessage(testString, mType);

        // Assert
        Assert.AreEqual(mType, logMessage.messageType);
        Assert.AreEqual(testString, logMessage.text);
	
	    yield return null;
    }

    [UnityTest]
    public IEnumerator WhenLogMessageIsGivenErrorMessageData_ExpectChangeInMessageType()
    {
        // Arrange
        ILogMessage logMessage = new LogMessage();
        MessageType mType = MessageType.Error;
        string testString = "This is a test.";

        // Act
        logMessage.SetMessage(testString, mType);

        // Assert
	    Assert.AreEqual(testString, logMessage.text);
        Assert.AreEqual(mType, logMessage.messageType);
	
	    yield return null;
    }

    [UnityTest]
    public IEnumerator WhenLogMessageIsGivenErrorMessageData_ExpectChangeInColor()
    {
        // Arrange
        ILogMessage logMessage = new LogMessage();
        Color32 initialColor = logMessage.color;
        MessageType mType = MessageType.Error;
        string testString = "This is a test.";

        // Act
        logMessage.SetMessage(testString, mType);

        // Assert
	    Assert.AreEqual(testString, logMessage.text);
        Assert.AreNotEqual(initialColor, logMessage.color);
	
	    yield return null;
    }

    [UnityTest]
    public IEnumerator CanLoadLogMessagePrefabFromResourceFolder()
    {
        // Arrange

        // Act
        var resource = Resources.Load("LogMessagePrefab");

        // Assert
        Assert.NotNull(resource);
	
	    yield return null;
    }

    [UnityTest]
    public IEnumerator WhenLogMessageInstanceIsGivenErrorMessageData_ExpectChangeInColor()
    {
        // Arrange
        GameObject obj = GameObject.Instantiate(Resources.Load("LogMessagePrefab", typeof(GameObject))) as GameObject;
        ILogMessage logMessage = obj.GetComponent<LogMessageInstance>();
        Color32 initialColor = logMessage.color;
        MessageType mType = MessageType.Error;
        string testString = "This is a test.";

        // Act
        logMessage.SetMessage(testString, mType);

        // Assert
	    Assert.AreEqual(testString, logMessage.text);
        Assert.AreNotEqual(initialColor, logMessage.color);
	
	    yield return null;
    }

     [UnityTest]
    public IEnumerator CanLoadActionLogViewPrefabFromResourceFolder()
    {
        // Arrange

        // Act
        var resource = Resources.Load("ActionLogView");

        // Assert
        Assert.NotNull(resource);
	
	    yield return null;
    }

    [UnityTest]
    public IEnumerator WhenActionLogViewPrefabLoaded_ExpectNoLogMessageChildObjects()
    {
        // Arrange
        GameObject obj = GameObject.Instantiate(Resources.Load("ActionLogView", typeof(GameObject))) as GameObject;

        // Act
        ILogMessage logMessage = obj.GetComponentInChildren<ILogMessage>();

        // Assert
	    Assert.Null(logMessage);
	
	    yield return null;
    }

    [UnityTest]
    public IEnumerator WhenMessageAddedToActionLogView_ExpectNewChildObject()
    {
        // Arrange
        GameObject obj = GameObject.Instantiate(Resources.Load("ActionLogView", typeof(GameObject))) as GameObject;
        ActionLogView actionLogView = obj.GetComponent<ActionLogView>();
        MessageType mType = MessageType.Error;
        string testString = "This is a test.";

        // Act
        actionLogView.AddEntry(testString, mType);
        ILogMessage logMessage = obj.GetComponentInChildren<ILogMessage>();

        // Assert
	    Assert.NotNull(logMessage);
	
	    yield return null;
    }
}
